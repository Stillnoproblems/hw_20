// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE_SnakeElemBP_generated_h
#error "SnakeElemBP.generated.h already included, missing '#pragma once' in SnakeElemBP.h"
#endif
#define SNAKE_SnakeElemBP_generated_h

#define Snake_Source_Snake_SnakeElemBP_h_15_SPARSE_DATA
#define Snake_Source_Snake_SnakeElemBP_h_15_RPC_WRAPPERS
#define Snake_Source_Snake_SnakeElemBP_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Snake_Source_Snake_SnakeElemBP_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeElemBP(); \
	friend struct Z_Construct_UClass_ASnakeElemBP_Statics; \
public: \
	DECLARE_CLASS(ASnakeElemBP, ASnakeElementBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(ASnakeElemBP)


#define Snake_Source_Snake_SnakeElemBP_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnakeElemBP(); \
	friend struct Z_Construct_UClass_ASnakeElemBP_Statics; \
public: \
	DECLARE_CLASS(ASnakeElemBP, ASnakeElementBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(ASnakeElemBP)


#define Snake_Source_Snake_SnakeElemBP_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeElemBP(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeElemBP) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeElemBP); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeElemBP); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeElemBP(ASnakeElemBP&&); \
	NO_API ASnakeElemBP(const ASnakeElemBP&); \
public:


#define Snake_Source_Snake_SnakeElemBP_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeElemBP() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeElemBP(ASnakeElemBP&&); \
	NO_API ASnakeElemBP(const ASnakeElemBP&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeElemBP); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeElemBP); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnakeElemBP)


#define Snake_Source_Snake_SnakeElemBP_h_15_PRIVATE_PROPERTY_OFFSET
#define Snake_Source_Snake_SnakeElemBP_h_12_PROLOG
#define Snake_Source_Snake_SnakeElemBP_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_SnakeElemBP_h_15_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_SnakeElemBP_h_15_SPARSE_DATA \
	Snake_Source_Snake_SnakeElemBP_h_15_RPC_WRAPPERS \
	Snake_Source_Snake_SnakeElemBP_h_15_INCLASS \
	Snake_Source_Snake_SnakeElemBP_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Source_Snake_SnakeElemBP_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_SnakeElemBP_h_15_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_SnakeElemBP_h_15_SPARSE_DATA \
	Snake_Source_Snake_SnakeElemBP_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_Source_Snake_SnakeElemBP_h_15_INCLASS_NO_PURE_DECLS \
	Snake_Source_Snake_SnakeElemBP_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_API UClass* StaticClass<class ASnakeElemBP>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_Source_Snake_SnakeElemBP_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
