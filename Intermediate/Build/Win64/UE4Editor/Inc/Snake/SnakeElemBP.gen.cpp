// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Snake/SnakeElemBP.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSnakeElemBP() {}
// Cross Module References
	SNAKE_API UClass* Z_Construct_UClass_ASnakeElemBP_NoRegister();
	SNAKE_API UClass* Z_Construct_UClass_ASnakeElemBP();
	SNAKE_API UClass* Z_Construct_UClass_ASnakeElementBase();
	UPackage* Z_Construct_UPackage__Script_Snake();
// End Cross Module References
	void ASnakeElemBP::StaticRegisterNativesASnakeElemBP()
	{
	}
	UClass* Z_Construct_UClass_ASnakeElemBP_NoRegister()
	{
		return ASnakeElemBP::StaticClass();
	}
	struct Z_Construct_UClass_ASnakeElemBP_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASnakeElemBP_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ASnakeElementBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Snake,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeElemBP_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "SnakeElemBP.h" },
		{ "ModuleRelativePath", "SnakeElemBP.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASnakeElemBP_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASnakeElemBP>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASnakeElemBP_Statics::ClassParams = {
		&ASnakeElemBP::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASnakeElemBP_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeElemBP_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASnakeElemBP()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASnakeElemBP_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASnakeElemBP, 4137401633);
	template<> SNAKE_API UClass* StaticClass<ASnakeElemBP>()
	{
		return ASnakeElemBP::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASnakeElemBP(Z_Construct_UClass_ASnakeElemBP, &ASnakeElemBP::StaticClass, TEXT("/Script/Snake"), TEXT("ASnakeElemBP"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASnakeElemBP);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
